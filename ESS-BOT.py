#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from web3 import Web3
import schedule
import time
import os
import json
import requests
import datetime
import pandas as pd
from pycoingecko import CoinGeckoAPI
from decimal import Decimal, ROUND_HALF_UP
import uuid
from csv import DictReader

# ENABLE LOGGING -

logging.basicConfig(filename='bugout_bot.log',
                    filemode='a',
                    level=logging.INFO,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger = logging.getLogger(__name__)

# Bot Config - Please see README.md for details on how to adjust/tune these
production_mode = True  # will not attempt to make trades unless this is True
sell_base_trigger = 4  # %
portion_of_bankroll = 2  # % of bankroll that we sell at base trigger
total_bankroll = 0  # if > 0, bot will assume you have ETH in cold storage
slippage = 1  # 1 Inch slippage
frequency = 1  # in minutes that bot loops through and checks ETH price
# given the opportunity, how much of a base sale should we buy back with
buy_back_size = 50  # % of total base order amount to re-buy with
# should we reset the base_eth_price if we drop sell_base_trigger DOWN (yes is more risky, also more action)
ride_down_wave = True


# probably don't need to change
# script will fail if base price of ETH is less than safePrice
safePrice = Decimal('120.00')
two_places = Decimal('0.01')  # used for Decimal rounding
trade_data = {}

# connection to CoinGecko API
try:
    cg = CoinGeckoAPI()
except:
    logger.warning(
        "Unable to make initial connection to CoinGecko API. Are you connected to the internet?")
    quit()

# Make sure the necessary environment variables are set
if 'ETH_PROVIDER_URL' in os.environ:
    eth_provider_url = os.environ['ETH_PROVIDER_URL']
else:
    logger.exception(
        "Please set ETH_PROVIDER_URL environment variable so ES2-BOT can connection to an Ethereum node!")
    quit()

if 'BASE_ACCOUNT' in os.environ:
    base_account = os.environ['BASE_ACCOUNT']
else:
    logger.exception(
        "BASE_ACCOUNT environment variable is required to run ES2-BOT!")
    quit()

if 'PRIVATE_KEY' in os.environ:
    private_key = os.environ["PRIVATE_KEY"]
else:
    logger.warning(
        'No private key has been set. Bot will not be able to send transactions!')
    private_key = False

# Establish Ethereum provider connection
# must establish a connection initally or bot will self destruct
try:
    web3 = Web3(Web3.HTTPProvider(eth_provider_url))
except:
    logger.warning("There is an issue with your initial connection to Ethereum Provider: {0}".format(
        eth_provider_url))
    quit()


def get_1_inch_data(current_balance):
    '''
    Query the 1Inch API and get call data needed to make a base trade
    '''
    global trade_data
    try:
        quote = requests.get(
            'https://api.1inch.exchange/v1.1/quote?fromTokenSymbol=ETH&toTokenSymbol=DAI&amount={0}'.format(current_balance))
        logger.info('1inch quote reply status code: {0}'.format(
            quote.status_code))
        if quote.status_code != 200:
            return False

        # estimate is easier than other options for now
        quote_data = quote.json()
        # Build out trade_data dict, adding first field
        trade_data = {
            "to_coin_amount": quote_data["toTokenAmount"]
        }

    except requests.exceptions.RequestException as e:
        # how to fail here? make trade or no? soft fail for now...
        logger.warning(
            "There was an issue getting quote request from 1Inch: {0}".format(e))
        return False

    # we only want to trade with a portion of our balance
    send_value = current_balance * portion_of_bankroll//100
    try:
        call_data = requests.get(
            'https://api.1inch.exchange/v1.1/swap?fromTokenSymbol=ETH&toTokenSymbol=DAI&amount={0}&fromAddress={1}&slippage={2}'.format(send_value, base_account, slippage))
        logger.info('1inch call_data reply status code: {0}'.format(
            call_data.status_code))
        if call_data.status_code != 200:
            return False

    except requests.exceptions.RequestException as e:
        logger.warning(
            "There was a issue getting call data from 1Inch: {0}".format(e))
        return False
    return call_data.json()


def send_to_1_inch(call_data):
    '''
    Craft transaction and send to 1 Inch exchange to make the base trade
    '''
    global trade_data
    if call_data and private_key:
        logger.info("---->building transaction for 1 Inch Exchange!")
        nonce = web3.eth.getTransactionCount(base_account)

        tx = {
            'nonce': nonce,
            'to': call_data['to'],
            'value': int(Decimal(call_data['value'])),
            'gas': int(Decimal(call_data['gas'])),
            'gasPrice': int(Decimal(call_data['gasPrice'])),
            'data': call_data['data'],
        }

        try:
            signed_tx = web3.eth.account.signTransaction(tx, private_key)
        except:
            logger.exception("Failed to created signed TX!")
            return False
        try:
            tx_hash = web3.eth.sendRawTransaction(signed_tx.rawTransaction)
            logger.info("TXID from 1 Inch: {0}".format(web3.toHex(tx_hash)))
        except:
            logger.warning("Failed sending TX to 1 inch!")
            return False

        # all is well, add necessary datas to our trade_data dictionary
        trade_data["date"] = str(datetime.datetime.now(datetime.timezone.utc))
        trade_data["from_coin"] = "ETH"
        trade_data["move"] = "SELL"
        trade_data["to_coin"] = "DAI"
        trade_data["from_coin_amount"] = int(call_data['value'])
        trade_data["txid"] = web3.toHex(tx_hash)

        return True

    else:
        logger.info(
            "Missing transaction call data and/or private key. No TX will be sent!")
        return False


def get_trade_call_data(from_coin, to_coin, amount):
    '''
    Generic function for getting Ethereum tx call_data from 1 Inch Exchange 
    '''
    ''' eventually it would be a good idea to confirm quoted price is equal to what we're aiming for. right now we take what ever 1 inch offers which is not safe
    try:
        quote = requests.get(
            'https://api.1inch.exchange/v1.1/quote?fromTokenSymbol={0}&toTokenSymbol={1}&amount={3}'.format(from_coin, to_coin, amount))
        logger.info('1inch quote reply status code: {0}'.format(
            quote.status_code))

    except requests.exceptions.RequestException as e:
        logger.warning(
            "There was an issue getting trade call data from 1 Inch: {0}".format(e))
        return False 
    '''
    try:
        call_data = requests.get(
            'https://api.1inch.exchange/v1.1/swap?fromTokenSymbol={0}&toTokenSymbol={1}&amount={2}&fromAddress={3}&slippage={4}'.format(from_coin, to_coin, amount, base_account, slippage))
        logger.info('response from 1 inch generic call_data request - status code: {0}'.format(
            call_data.status_code))
        if call_data.status_code != 200:
            logger.info("non 200 response from 1 inch! This is probably bad.")
            return False

    except requests.exceptions.RequestException as e:
        logger.warning(
            "There was a issue getting generic call data from 1 inch: {0}".format(e))
        return False

    return call_data.json()


def broadcast_1_inch_tx(call_data):
    '''
    Craft generic transaction and send to 1 Inch exchange
    '''

    if call_data and private_key:
        logger.info("---->building transaction for 1 Inch Exchange!")
        nonce = web3.eth.getTransactionCount(base_account)

        tx = {
            'nonce': nonce,
            'to': call_data['to'],
            'value': int(Decimal(call_data['value'])),
            'gas': int(Decimal(call_data['gas'])),
            'gasPrice': int(Decimal(call_data['gasPrice'])),
            'data': call_data['data'],
        }

        try:
            signed_tx = web3.eth.account.signTransaction(tx, private_key)
        except:
            logger.exception("Failed to created signed TX!")
            return False
        try:
            tx_hash = web3.eth.sendRawTransaction(signed_tx.rawTransaction)
            logger.info("TXID from 1 Inch: {0}".format(web3.toHex(tx_hash)))
        except:
            logger.warning("Failed sending TX to 1 inch!")
            return False

        return web3.toHex(tx_hash)

    else:
        logger.info(
            "Missing transaction call data and/or private key. No TX will be sent!")
        return False


def get_eth_usd():
    '''
    Retrieve ETH price in USD from Coin Gecko API
    '''
    # logger.info("Coin Gecko ping: {0}".format(cg.ping()))
    try:
        current_eth_price = cg.get_price(ids='ethereum', vs_currencies='usd')
    except:
        logger.warning("Failed to get current ETH price from CoinGecko!")
        # gecko_error_count += 1
        return False

    # logger.info("current Ethereum price from CoinGecko API: {0}".format(current_eth_price))
    return current_eth_price["ethereum"]["usd"]


def get_balance(account):
    '''
    Retrieve balance of an account from eth_provider_url
    '''
    balance = web3.eth.getBalance(account)
    # log balance in Ether unit...
    logger.info("ETH balance on base account: {0}".format(
        web3.fromWei(balance, "ether")))
    return balance  # in WEI!


def calculate_change(current_eth_price, base_eth_price):
    '''
    Calculate and return % of change between current price and base price.
    '''
    if current_eth_price == base_eth_price:  # price has not changed
        logger.info("No price change since last check!")
        return 0

    if current_eth_price > base_eth_price:  # price has increased
        price_change = Decimal(((current_eth_price - base_eth_price) /
                                base_eth_price) * 100).quantize(two_places, ROUND_HALF_UP)
        logger.info(
            "Price has increased by: +{0}%".format(price_change))
        return price_change

    if current_eth_price < base_eth_price:  # price has dropped
        price_change = Decimal(((base_eth_price - current_eth_price) /
                                base_eth_price) * 100).quantize(two_places, ROUND_HALF_UP)
        logger.info(
            "Price has decreased by: -{0}%".format(price_change))
        return -(price_change)


def write_base_trade():
    '''
    Used to write the base trade to local csv
    '''
    global trade_data
    # order our data
    desired_order = ['date', 'from_coin', 'move', 'to_coin', 'base_coin_price', 'sale_price',
                     'from_coin_amount', 'to_coin_amount', 'percentage_change', 'percentage_of_bankroll', 'txid', 'buy_back_status']
    reordered_trade_data = {k: trade_data[k] for k in desired_order}
    formatted_trade_data = list(reordered_trade_data.values())
    try:
        with open('trades.csv') as f:
            df = pd.DataFrame([formatted_trade_data])
            df.to_csv(r'trades.csv', sep=',',
                      header=False, index=False, mode='a')

    except IOError:  # first time running script or trades file has been deleted
        logger.info("trades.csv file not found! Creating...")
        df = pd.DataFrame([formatted_trade_data], columns=['date', 'from_coin', 'move', 'to_coin', 'base_coin_price',
                                                           'sale_price', 'from_coin_amount', 'to_coin_amount', 'percentage_change', 'percentage_of_bankroll', 'txid', 'buy_back_status'])
        df.to_csv(r'trades.csv', sep=',', index=False, mode='a')


def write_buy_back_order(buy_back_order):
    '''
    Used to write an active buy back order to the csv file
    '''
    # prep buy back order to write to disk
    formatted_buy_back_order = list(buy_back_order.values())
    try:
        with open('open_buy_back_orders.csv') as f:
            df = pd.DataFrame([formatted_buy_back_order])
            df.to_csv(r'open_buy_back_orders.csv', sep=',',
                      header=False, index=False, mode='a')
        logger.info(
            "Buy back order successfully written to buy_back_order.csv!")

    except IOError:  # first time running script or trades file has been deleted
        logger.info("open_buy_back_orders.csv file not found! Creating...")
        df = pd.DataFrame([formatted_buy_back_order], columns=['uuid', 'date', 'from_coin', 'move', 'to_coin', 'base_coin_price',
                                                               'sale_price', 'from_coin_amount', 'price_usd', 'to_coin_amount', 'percentage_change', 'percentage_of_bankroll', 'this_txid', 'og_txid', 'buy_back_status'])
        df.to_csv(r'open_buy_back_orders.csv', sep=',', index=False, mode='a')


def craft_buy_back_order(og_trade_DAI_amount, txid, buy_back_position, current_eth_price):
    '''
    This will craft an active buy back order so it can be written to disk
    '''
    # how much DAI should we sell?
    # We'll only allocate buy_back_size % of og trade, from there we set this trades from_coin_amount, ie the total DAI this trade will use
    buy_back_amount = round(int(og_trade_DAI_amount) *
                            int(buy_back_size)/100 * int(buy_back_position)/100)

    # static for now, dynamic down the road as needed
    from_coin = "DAI"
    to_coin = "ETH"

    buy_back_trade_order = {
        "uuid": uuid.uuid4(),
        "date": str(datetime.datetime.now(datetime.timezone.utc)),
        "from_coin": from_coin,
        "move": "SELL",
        "to_coin": to_coin,
        "base_coin_price": 0,
        "sale_price": 0,
        "from_coin_amount": buy_back_amount,
        "price_usd": int(current_eth_price) - (int(current_eth_price) * (buy_back_position / 100)),
        "to_coin_amount": 0,
        "percentage_of_change": -int(buy_back_position),
        "percentage_of_bankroll": "NA",
        "this_txid": "TBD",
        "og_txid": txid,
        "buy_back_status": "OPEN"
    }

    return write_buy_back_order(buy_back_trade_order)

# takes dict of completed order, writes it to disk


def craft_buy_back_completed(total_buy_back_amount, orders_filled):

    completed_trade_data = {
        "uuid": uuid.uuid4(),
        "date": str(datetime.datetime.now(datetime.timezone.utc)),
        "order_amount": total_buy_back_amount,
        "orders_filled": orders_filled,
        "txid": "TBD"
    }
    return completed_trade_data


def execute_buy_back_trade(order_data_completed):
    logger.info('Attempting to execute trade!')
    try:
        # if we get call data from 1 inch, then fire off the trade
        call_data = get_trade_call_data(
            "DAI", "ETH", order_data_completed["order_amount"])
        if call_data:
            txid = broadcast_1_inch_tx(call_data)
        else:
            logger.info(
                "There was an issue getting call data. No trade has been placed!")
            return False
    except Exception as e:
        logger.warning(
            "There was an issue getting call data and/or executing buy back trade: {0}".format(e))
        return False

    order_data_completed["txid"] = txid
    return order_data_completed


def write_buy_back_completed(completed_trade_data):
    formatted_completed_trades = list(completed_trade_data.values())
    try:
        with open('completed_buy_back_trades.csv') as f:
            df = pd.DataFrame([formatted_completed_trades])
            df.to_csv(r'completed_buy_back_trades.csv', sep=',',
                      header=False, index=False, mode='a')
        logger.info(
            "Buy back trade successfully written to completed_buy_back_trades.csv!")
        return True
    except IOError:  # file does not exist - either first time needing this file, or file has been deleted
        logger.info("completed buy back trades file not found! Creating...")
        df = pd.DataFrame([formatted_completed_trades], columns=[
                          'uuid', 'date', 'order_amount', 'orders_filled', 'txid'])
        df.to_csv(r'completed_buy_back_trades.csv',
                  sep=',', index=False, mode='a')
        return True


def close_completed_orders(orders_filled):
    '''
    Delete active trades from the buy back trades file
    '''
    try:
        for uuid in orders_filled:
            with open("open_buy_back_orders.csv", "r") as f:
                lines = f.readlines()
            with open("open_buy_back_orders.csv", "w") as f:
                # skip first line of file as it contains the headers
                # iterate through lines and remove any lines with matching uuid
                line_count = 0
                for line in lines:
                    if line_count != 0:
                        if line.strip("\n").find(uuid) == -1:
                            f.write(line)
                    else:
                        f.write(line)
                    line_count += 1
        logger.info('Removed open buy back orders from open_buy_back_orders.csv: {0}'.format(
            orders_filled))
        return True
    except Exception as e:
        logger.warning(
            "Error removing buy back orders from disk! This could cause serious issues! {0}".format(e))
        return False


def check_buy_back_orders(_current_eth_price):
    orders_filled = []
    total_buy_back_amount = 0
    with open('open_buy_back_orders.csv', 'r') as read_obj:
        buy_back_orders = DictReader(read_obj)
        for trade_order in buy_back_orders:
            usd_buy_back_price = Decimal(trade_order["price_usd"]).quantize(
                two_places, ROUND_HALF_UP)
            # iterate through active buy back orders, take action on any orders that hit
            # logger.info("comparing prices: {0} vs {1}".format(
            #    usd_buy_back_price, _current_eth_price))
            if trade_order["buy_back_status"] == "OPEN" and usd_buy_back_price >= _current_eth_price:
                # qualifying order found, add to list
                logger.info("open buy back order {0} has been matched! here we go...".format(
                    trade_order["uuid"]))
                orders_filled.append(trade_order["uuid"])
                # keep a running total of total DAI across all qualifying orders
                total_buy_back_amount = total_buy_back_amount + \
                    int(trade_order["from_coin_amount"])
    # Taking action on any orders that hit - make buy back trade!
    if total_buy_back_amount > 0:
        # close open trades, craft our order dict, execute the trade, write trade to log
        try:
            # craft order dict
            order_completed_data = craft_buy_back_completed(
                total_buy_back_amount, orders_filled)
            # execute trade
            if order_completed_data:
                updated_order_completed_data = execute_buy_back_trade(
                    order_completed_data)
            # write trade log to disk (completed_buy_back_trades.csv)
            if updated_order_completed_data:
                write_return = write_buy_back_completed(
                    updated_order_completed_data)
            # remove open trades from file
            if write_return:
                close_completed_result = close_completed_orders(orders_filled)
            # if we failed to close open trades we're going to stop the bot
            if close_completed_result != True:
                logger.warning("Open trade improperly cleared! quitting...")
                quit()  # we must avoid infinite sell loop
        except Exception as e:
            logger.warning(
                "Failed crafting, sending, or writing buy back order to disk!\n {0}".format(e))
            return False
    else:
        logger.info("No open & matching buy back orders were found!")


def run_bot_loop():
    '''
    Checks Ethereum provider connection to make sure we're on track then starts collecting data
    '''
    global base_eth_price
    try:
        current_block_number = web3.eth.blockNumber
        logger.info("BEGIN LOOP-->*****************************")
        logger.info("Connected to Ethereum provider: {0}".format(
            web3.isConnected()))
        logger.info("Most recent block: {0}".format(current_block_number))
    except:
        logger.warning(
            "There appears to be an issue with your connection to web3 Ethereum provider!")
        return
    change = 0  # change starts at 0
    current_balance = get_balance(base_account)  # get base account balance
    # returns current ETH price in USD
    current_eth_price = Decimal(get_eth_usd()).quantize(
        two_places, ROUND_HALF_UP)
    if current_eth_price == False:
        logger.warning(
            "Skipping the rest of the loop this time because we don't have an updated ETH price!")
        return
    logger.info("BASE_PRICE: {0} CURRENT_ETH_PRICE: {1}".format(
        base_eth_price, current_eth_price))
    # how much has the price changed (+|-) from the base price?
    change = calculate_change(current_eth_price, base_eth_price)
    logger.info("Checking to see if we should make a base coin sale...")
    # if price has increased enough, then we'll make a base trade
    if change > sell_base_trigger:
        if production_mode:
            logger.info(
                "Base sale trigger hit! Time to sell ETH! Price has increased by: {0}".format(change))
            # get all data, and send broadcast trade TX, and set new base_eth_price
            if send_to_1_inch(get_1_inch_data(current_balance)):
                # update trade_data dict
                trade_data["base_coin_price"] = int(base_eth_price)
                trade_data["sale_price"] = int(current_eth_price)
                trade_data["percentage_change"] = int(sell_base_trigger)
                trade_data["percentage_of_bankroll"] = int(portion_of_bankroll)
                trade_data["buy_back_status"] = 0
                # write trade to disk
                write_base_trade()
                base_eth_price = current_eth_price

                # generate buy back points (experiemental)
                point_1 = sell_base_trigger
                point_2 = sell_base_trigger * 2
                point_3 = point_1 + point_2
                buy_back_points = [point_1, point_2, point_3]

                # craft & write buy back trades to disk
                for buy_back_position in buy_back_points:
                    craft_buy_back_order(
                        trade_data["to_coin_amount"], trade_data["txid"], buy_back_position, current_eth_price)
            else:
                logger.warning(
                    "It appears there was an issue getting necessary info from 1 Inch! Trade likely failed!")
        else:
            logger.info(
                "Base sale trigger hit but we're not in production mode!! Price has increased by: {0}".format(change))
            base_eth_price = current_eth_price

    # check if buy back trade is in order, execute trade if so
    try:
        logger.info("Checking open buy back orders for matches...")
        check_buy_back_orders(current_eth_price)

    except Exception as e:
        logger.warning(
            "There was an issue checking open buy back orders!\n {0}".format(e))
    # if price has decreased the equivalent amount of sell_base_trigger, then we'll adjust the base_eth_price
    if ride_down_wave and change < -(sell_base_trigger):
        logger.info("Down wave condition encountered! Sad day. base_sell_price has been adjusted to: {0}".format(
            current_eth_price))
        base_eth_price = current_eth_price

    logger.info("END LOOP-->*****************************\n")


def main():
    global base_eth_price
    # run once on start
    logger.info("ETHEREUM-STABLE-STRAT-V1 --> IN_MATHS_WE_TRUST --> LIFT OFF!!")

    # Setting BASE_ETH_PRICE is optional. Bot will grab current price and use that if envar is not set
    if 'BASE_ETH_PRICE' in os.environ:
        base_eth_price = Decimal(
            os.environ['BASE_ETH_PRICE']).quantize(two_places, ROUND_HALF_UP)
    else:
        # logger.info("Getting base_eth_price from Coin Gecko!!")
        try:
            base_eth_price = Decimal(
                get_eth_usd()).quantize(two_places, ROUND_HALF_UP)
            assert (base_eth_price >= safePrice), "Ethereum price seems off! This is a safety measure to ensure we do not get the wrong base price."
        except:
            logger.warning(
                "Failed to get base from Coin Gecko! That's going to be a problem. Are you online?")
            quit()

    # kick things off
    run_bot_loop()

    # then start our scheduled loop
    schedule.every(frequency).minutes.do(run_bot_loop)
    while True:
        schedule.run_pending()
        time.sleep(1)


if __name__ == '__main__':
    main()
